module.exports = {
  branches: [
    "main"
  ],
  ci: false,
  plugins: [
    [
      "@semantic-release/commit-analyzer",
      {
        preset: "conventionalcommits",
        releaseRules: [
          { breaking: true, release: "major" },
          { type: "feat", release: "minor" },
          { revert: true, release: "patch" },
          { type: "build", release: "patch" },
          { type: "ci", release: "patch" },
          { type: "docs", release: "patch" },
          { type: "fix", release: "patch" },
          { type: "perf", release: "patch" },
          { type: "refactor", release: "patch" },
          { type: "revert", release: "patch" },
          { type: "style", release: "patch" },
          { type: "test", release: "patch" },
          { type: "chore", release: false }
        ]
      }
    ],
    [
      // In order to match the custom releaseRules, this module is patched during the build process.
      "@semantic-release/release-notes-generator",
      {
        preset: "conventionalcommits"
      }
    ],
    [
      "@semantic-release/changelog",
      {
        changelogFile: "CHANGELOG.md"
      }
    ],
    [
      "@semantic-release/exec",
      {
        prepareCmd: "if [ -f VERSION ]; then echo ${nextRelease.version} > VERSION; fi; if [ -x version.sh ]; then ./version.sh ${nextRelease.version}; fi"
      }
    ],
    [
      "@semantic-release/git",
      {
        assets: "**",
        message: "chore(release): ${nextRelease.version}"
      }
    ],
    [
      "@semantic-release/gitlab",
      {
        failComment: false,
        failTitle: false,
        labels: false,
        successComment: false
      }
    ]
  ]
}
