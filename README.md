# omnitoolkit - releasebot

This program analyzes a git repository and, if necessary, performs a release according to the specifications of  [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) and [semantic versioning](https://semver.org/spec/v2.0.0.html).

It uses [semantic-release](https://github.com/semantic-release/semantic-release) (see [documentation](https://semantic-release.gitbook.io/semantic-release)) inside a Docker image with a custom-made module (see [index.js](src/index.js)). There, several plugins along with their configuration are defined and loaded on execution:

- [@semantic-release/commit-analyzer](https://github.com/semantic-release/commit-analyzer) reads each git commit added since the last release, determines if a new release is due, the type of release and the new version number. It analyzes the syntax according to the conventionalcommits preset in [parser-opts.js](https://github.com/conventional-changelog/conventional-changelog/blob/master/packages/conventional-changelog-conventionalcommits/parser-opts.js) and checks for custom rules combined from the [default rules list](https://github.com/semantic-release/commit-analyzer/blob/master/lib/default-release-rules.js) and the [@commitlint/config-conventional type list](https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-conventional#type-enum). A commit should look like this:
  ```
  <type>(<scope>)!: <short summary>
  │       │      │       │
  │       │      │       └─> Summary in present tense. Not capitalized. No period at the end.
  │       │      │
  │       │      └─> Indicator for breaking change. Optional.
  │       │
  │       └─> Commit scope. Can be any string. Optional.
  │
  └─> Commit type:    build|chore|ci|docs|feat|fix|perf|refactor|revert|style|test
      No release:             x
      Patch release:    x         x   x         x   x      x       x      x    x
      Minor release:                       x
      Major release:            (any type when used with '!' indicator)
  ```
  (chart from [angular](https://github.com/angular/angular/blob/main/CONTRIBUTING.md#commit-message-header), changed)
- [@semantic-release/release-notes-generator](https://github.com/semantic-release/release-notes-generator) generates new release notes from the collected information according to the conventionalcommits preset in [writer-opts.js](https://github.com/conventional-changelog/conventional-changelog/blob/master/packages/conventional-changelog-conventionalcommits/writer-opts.js).
- [@semantic-release/changelog](https://github.com/semantic-release/changelog) updates the changelog file `CHANGELOG.md` with the generated release notes.
- [@semantic-release/exec](https://github.com/semantic-release/exec) updates the file `VERSION` with the new version number (only if `VERSION` exists) and executes the command `./version.sh X.Y.Z` (only if `version.sh` exists). Both are optional, the latter can be used to set the new version number in other files in the repository.
- [@semantic-release/git](https://github.com/semantic-release/git) adds a new git commit with the above file changes, places a new git tag (new version number with prefixed `v`) and pushes the changes to the git remote.
- [@semantic-release/gitlab](https://github.com/semantic-release/gitlab) creates a GitLab release via API with the generated release notes.

## Requirements

- Git repository hosted on GitLab SaaS or self-managed
- GitLab CI/CD enabled and variable `GITLAB_TOKEN` set (can either be a personal, project or group access token with the scopes `api` and `write_repository`)
- Willingness of all contributors to follow the commit syntax above

## Usage

- In your `.gitlab-ci.yml`, define the stages explicitly and add a `release` stage at the end of the list. This ensures that the release runs only on a (to this point) successful pipeline.
  ```yaml
  stages:
    - build
    - test
    - deploy
    - release
  ```
- Then add the following job and replace `main` with the desired version from the [releases](https://gitlab.com/omnitoolkit/releasebot/-/releases) page:
  ```yaml
  release_version:
    image: registry.gitlab.com/omnitoolkit/releasebot:main
    stage: release
    rules:
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    script:
      - releasebot --branches ${CI_DEFAULT_BRANCH}
  ```
- If `CI_DEFAULT_BRANCH` is not your release branch, replace it with the correct one.
- If you don't want to release on every push to the release branch, add `when: manual` to the job. You can now start the job on demand from the UI.
