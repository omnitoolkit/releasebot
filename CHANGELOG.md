## [2.0.0](https://gitlab.com/omnitoolkit/releasebot/compare/v1.1.1...v2.0.0) (2022-11-22)


### ⚠ BREAKING CHANGES

* add release for commit type "style" and "test"

### Features

* add release for commit type "style" and "test" ([7b9c67e](https://gitlab.com/omnitoolkit/releasebot/commit/7b9c67e8ac7de419844dde3f223b424c71434143))
* pin to specific node image and npm packages ([3ebe517](https://gitlab.com/omnitoolkit/releasebot/commit/3ebe517aa6268c6a0446a44a273b6f0288b8f854))


### Continuous Integration

* use default image tag for own release ([f08b61c](https://gitlab.com/omnitoolkit/releasebot/commit/f08b61cd353d5e8b48ab22be66a8fc013271a68a))


### Documentation

* add requirements and usage to README ([c4395ad](https://gitlab.com/omnitoolkit/releasebot/commit/c4395ad54bcbf8ec79ca87298db0822f92081466))

## [1.1.1](https://gitlab.com/omnitoolkit/releasebot/compare/v1.1.0...v1.1.1) (2022-11-22)


### Bug Fixes

* show all relevant types in the release notes ([cd28fbe](https://gitlab.com/omnitoolkit/releasebot/commit/cd28fbebadb2ae19a267b254dabfe8d47502cb52))

## [1.1.0](https://gitlab.com/omnitoolkit/releasebot/compare/v1.0.2...v1.1.0) (2022-11-22)


### Features

* add exec plugin to set version string in files ([ddab6f2](https://gitlab.com/omnitoolkit/releasebot/commit/ddab6f2f90137d69ea0b3816eb23e6b5a6bd8135))

## [1.0.2](https://gitlab.com/omnitoolkit/releasebot/compare/v1.0.1...v1.0.2) (2022-11-08)


### Continuous Integration

* use release version from template ([afd3622](https://gitlab.com/omnitoolkit/releasebot/commit/afd362204dac76d62210f246663c55d9b42f3f60))

## [1.0.1](https://gitlab.com/omnitoolkit/releasebot/compare/v1.0.0...v1.0.1) (2022-11-07)


### Bug Fixes

* remove whitespaces ([4903625](https://gitlab.com/omnitoolkit/releasebot/commit/490362506234b6e59855c114b33767f5f669f7fb))

## 1.0.0 (2022-11-06)


### Features

* add ci build of releasebot ([29f0e56](https://gitlab.com/omnitoolkit/releasebot/commit/29f0e568c3201eba43899232a05747833fd218f3))
* add ci release with releasebot ([ae2f0fa](https://gitlab.com/omnitoolkit/releasebot/commit/ae2f0fa8c6590bfed37bcdf9debaa7d1404ef3e7))
* add source files for releasebot ([ffef5d2](https://gitlab.com/omnitoolkit/releasebot/commit/ffef5d233853240844cd9935fb545bafd43dc3dd))
* remove update of VERSION file for now ([b2e63c5](https://gitlab.com/omnitoolkit/releasebot/commit/b2e63c524905f668f1e5224a21e085ef1a438e5b))


### Bug Fixes

* use CI_SERVER_URL as GITLAB_URL for ci ([4872e36](https://gitlab.com/omnitoolkit/releasebot/commit/4872e36e801414a2d7fcfc7f40eb32d2fd7d02a0))
